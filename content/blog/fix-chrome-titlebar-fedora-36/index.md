---
title: "Fix Chrome titlebar color in Fedora"
date: 2023-07-19T14:15:08-04:00
draft: false
tags: ["google", "chrome", "issues", "fix", "workaround", "fedora", "linux"]
---

If you come across the issue of wanting Chrome to use dark mode but the title bar is still bright white, read on...

### Issue found in Fedora 36

I have not had experience with this problem in other versions.

## How to fix this issue

Original solution found [here](https://superuser.com/questions/1721753/google-chrome-does-not-detect-system-dark-theme-in-fedora-36-with-gnome).

Run the following commands:

```bash
sed -i 's/^Exec=\/usr\/bin\/google-chrome-stable$/& --enable-features=WebUIDarkMode --force-dark-mode/' /usr/share/applications/google-chrome.desktop
sed -i 's/%U/--enable-features=WebUIDarkMode --force-dark-mode &/' /usr/share/applications/google-chrome.desktop
```

It's a fairly easy fix which basically just forces Chrome to use dark mode.
