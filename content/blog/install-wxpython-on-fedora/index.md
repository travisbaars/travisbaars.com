---
title: "How to get wxPython running on Fedora 36/38"
date: 2023-07-08T20:30:38-04:00
draft: false
tags:
  [
    "open source",
    "software",
    "python",
    "wxpython",
    "linux",
    "fedora",
    "install",
    "walkthrough",
  ]
---

The instructions for installing wxPython on other platforms can be found [here](https://github.com/wxWidgets/Phoenix#id8). I usually run Fedora as my desktop OS and the lack of an easy way to install all the dependencies and such is what spurred this post.

#### I have run these steps on Fedora 36 and 38 successfully

## Clone wxPython repository

This step is not neccessary unless you are building from source code (which is not happening here).

```bash
git clone git@github.com:wxWidgets/Phoenix.git
```

## Install dependencies

### Install the Fedora equivalent to Ubuntu's `build-essentials`

```bash
sudo dnf groupinstall "Development Tools" "Development Libraries"
```

```bash
sudo dnf install make automake gcc gcc-c++ kernel-devel
```

### Fedora versions of required packages

```bash
sudo dnf install \
dpkg-dev \
python3-devel \
freeglut-devel \
mesa-libGL-devel \
mesa-libGLU \
gstreamer1-plugins-base-devel \
gtk3-devel \
libjpeg-turbo-devel \
libnotify-devel \
libpng-devel \
SDL2-devel \
libSM-devel \
libtiff-devel \
webkit2gtk4.0-devel \
libXtst-devel
```

## Install wxPython

It is recommended to install packages like wxPython in a virtual environment. To do this make sure you have the `python3-virtualenv` package installed.

### Install `virtualenv`

```bash
sudo dnf install python3-virtualenv
```

### Create new virtual environment

Navigate to preferred project directory path then create a new virtual environment with the following command:

```bash
virtualenv .venv
```

Activate the newly created virtual environment:

```bash
source .venv/bin/activate
```

### Update pip

```bash
pip install --upgrade pip
```

### Use wxPython build script to complete installation

Before continuing, the `attrdict3` package must be installed first.

```bash
pip install attrdict3
```

Then continue with the wxPython installation.

```bash
pip install wxpython
```

### If on low power computer and building wxPython fails

My laptop is a 10 year old Chromebook flashed with Fedora 36 and was not able to build the wxPython wheel. The solution I was able to find was to download the pre-built wheel from [official site](https://extras.wxpython.org/wxPython4/extras/linux/gtk3/fedora-36/) and install it that way.

#### Get wheel

```bash
wget https://extras.wxpython.org/wxPython4/extras/linux/gtk3/fedora-36/wxPython-4.2.0-cp310-cp310-linux_x86_64.whl
```

#### Install wheel

```bash
pip install wxPython-4.2.0-cp310-cp310-linux_x86_64.whl
```
