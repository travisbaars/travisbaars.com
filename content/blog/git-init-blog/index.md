---
title: "git init blog..."
date: 2023-05-21T11:57:58-04:00
draft: false
tags: ["open source", "software", "intro"]
---

## Welcome to my little piece of the internet

How cliche right?

### The introduction

First of all, I would like to take a moment to thank you for being here. I have no experience doing this sort of thing, but here it goes...

My name is Travis and I am many things. Name any hobby and I gauruntee I've either done it or thought about doing it. Things like music, archery, DIY electronics, programming, homelab, gaming, metalwork, building computers, 3d printing, and probably many more that I can't even remember. I like to be challenged; I like to solve problems. These traits lend themselves to many interesting projects across the board.

Now, you are probably wondering what we are both doing here...

### The background

Throughout all my hobbies and whatnot, I've discovered the importance of the open source community. There's nothing worse than getting neck deep into a project before realizing that you need to purchase some sort of proprietary thing, wether it be a small circuit board or a piece of software. You will find yourself feeling deflated and losing interest, but is that necessary? Perhaps not... As it turns out there are so many active (and even semi-inactive) projects out there! Sure some of them may lack some fancy features or not look as pretty, but they can be just as functional as their commercial counterparts. The open source community is a wonderful thing and if you didn't know already, you will soon!

### The story

Now over the years, I have incorporated many open source projects into projects of my own. Most of the time this is relatively straight forward; You follow the readme in a GitHub repository, or go to a projects Docementation site and you are up and running in no time. But what happens if what you are trying to do is not so straight forward? An edge case? Well if you are like me, you do the deepest Google dive trying to find the ONE person who wants to do the same thing. Its difficult especially when you are trying to combine the steps from two, or even more, tutorials and StackOverflow pages. It can be challenging and also rewarding.

For a while now I've been looking for a way to give back to the internet and the open source community. I wanted a place to share my finds and fixes to various problems and edge cases I've run across. A place with no frills, no un-needed life stories, just simple examples and solutions. I have no definite plan, and certainly no limit to the variety of content. I just want to give back in a way that I think could benefit fellow creators like myself.

### The exit

This is probably the most useless of all my posts, but its the best I could come up with as a first. To help anyone interested get an idea of what I am about. If you made it this far, congrats! You win nothing. Hopefully you can find what you are looking for here. If you have any questions feel free to ask!

Thanks,

&minus; T
